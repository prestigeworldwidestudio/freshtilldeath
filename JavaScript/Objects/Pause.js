var Pause = function(string, scores){
	// variable declaration
	this.string = string;
	this.window;
	this.tutorialText;
	this.screen = game.add.image(0, 0, 'screen');
    this.timeUnpaused;
    this.youWin;
	this.screen.alpha = 0.9;
    this.shoeback;
    this.shoefront;
    this.win_bg = game.add.image(game.width/2, game.height/2, 'shine');
    this.win_bg.scale.setTo(2);
    this.win_bg.anchor.setTo(0.5, 0.5);
    this.win_bg.alpha = 0;

	// draws unpause text on screen
	this.pauseText = game.add.text(game.width/2, game.height*(14/15),
			"PRESS SPACEBAR TO CONTINUE", {font: "36px Arial", fill: "#FFFFFF"});
	this.pauseText.anchor.setTo(0.5, 0.5);
    this.pauseText.alpha = 0.8;

	game.paused = true;
    if (scores != null){
        this.scores = scores;
    }
 	// starts tutorial
	if (this.string == 'tutorial'){
        this.startTutorial();
    }
    else if (this.string == 'score'){
        this.showScore();
    } else if (this.string == 'win'){
        this.showWin();
    }

	// captures spacebar presses and handles event
	game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR).onDown.add(Pause.prototype.unpause, this);
}

Pause.prototype.constructor = Pause;

Pause.prototype.showWin = function(){
    this.win_bg.alpha = 1;

	// displays the tutorial window
	this.window = game.add.image(game.width/2, game.height*(2/5), 'tutorial');
	this.window.anchor.setTo(0.5, 0.5);
	this.window.bringToTop();

    this.shoeback = this.createShoe();
    this.shoeback.x += 100;
    this.shoeback.y -= 50;
    this.shoefront = this.createShoe();
    this.shoefront.y -= 100;
    this.shoeback.y -= 100;

    var winText =  "Great job! You've been sponsored by \n every store and collected all of the shoes! \nKeep playing to add to your collection.";
    this.youWin = game.add.text(game.world.width/2, game.world.height/2, winText, {font: "42px Schoolbell", fill: "#FFFFFF", align: "center"});
    this.youWin.anchor.setTo(1, 0);
    this.youWin.x += this.youWin.width/2;

    this.pauseText.destroy();
},

Pause.prototype.showScore = function(){
    var shoeBack = this.createShoe();
    shoeBack.x += 100;
    shoeBack.y -= 50;
    var shoeTop = this.createShoe();
    shoeTop.y -= 100;
    shoeBack.y -= 100;

    var followers = this.scores[0];
    var cpmSocial = this.scores[1];
    var cpmWalk = this.scores[2];
    var shoeValue = this.scores[3];
    var freshness = this.scores[4];

    switch (game.state.current){
        case 'Walk':
            var winText;
            if (freshness >= 0.75){
                winText = "You got home safely! You can still \n clean your shoes to 150% freshness.";
            } else {
                winText = "You made it home, but your shoes got\n pretty dirty. Make sure you clean them!";
            }
            var youWin = game.add.text(game.world.width/2, game.world.height/2, winText, {font: "50px Schoolbell", fill: "#FFFFFF", align: "center"});
            youWin.anchor.setTo(1, 0);
            youWin.x += youWin.width/2;

            var cpmWalkText = game.add.text(game.world.width/2, youWin.y + youWin.height*1.5, "Your CPM: ", {font: "50px Schoolbell", fill: "#FFFFFF"});
            cpmWalkText.anchor.setTo(1, 0);
            cpmWalkText.x += cpmWalkText.width/2;
            var cpmWalk = game.add.text(cpmWalkText + 10, youWin.y + youWin.height*1.5, "" + cpmWalk, {font: "50px Schoolbell", fill: "#FFFFFF"});

            var shoeValueFresh = game.add.text(game.world.width/2, cpmWalkText.y + cpmWalkText.height, "Shoe Freshness: ", {font: "50px Schoolbell", fill: "#FFFFFF"});
            shoeValueFresh.anchor.setTo(1, 0);
            shoeValueFresh.x += cpmWalkText.width/2;
            var shoeValue = game.add.text(shoeValueFresh + 10, game.world.height/2, "" + Math.ceil(shoeValue * freshness) + "%", {font: "50px Schoolbell", fill: "#FFFFFF"});

            break;
        case 'Polish':
            var shoeValueFresh = game.add.text(game.world.width/2, game.world.height/2, "Shoe Freshness: ", {font: "50px Schoolbell", fill: "#FFFFFF"});
            shoeValueFresh.anchor.setTo(1, 0);
            shoeValueFresh.x += shoeValueFresh.width/2;
            var shoeValue = game.add.text(shoeValueFresh + 10, game.world.height/2, "" + Math.ceil(shoeValue * freshness) + "%", {font: "50px Schoolbell", fill: "#FFFFFF"});
            break;
		case 'SocialMedia':
			var socialCPM = game.add.text(game.world.width/2, game.world.height/2, "KixStarter post accepted!", {font: "50px Schoolbell", fill: "#FFFFFF"});
            socialCPM.anchor.setTo(1, 0);
			socialCPM.x += socialCPM.width/2;

			var socialCPM2 = game.add.text(game.world.width/2, socialCPM.y + socialCPM.height, "Your CPM: ", {font: "50px Schoolbell", fill: "#FFFFFF"});
            socialCPM2.anchor.setTo(1, 0);
			socialCPM2.x += socialCPM2.width/2;

            var socialValue = game.add.text(socialCPM + 10, socialCPM.y + socialCPM.height, "" + cpmSocial, {font: "50px Schoolbell", fill: "#FFFFFF"});
			break;
        case 'Hub':
            var cpmWalkText = game.add.text(game.world.width/2, game.world.height/2, "Your CPM in Walk: ", {font: "50px Schoolbell", fill: "#FFFFFF"});
            cpmWalkText.anchor.setTo(1, 0);
            cpmWalkText.x += cpmWalkText.width/2;
            var cpmWalk = game.add.text(cpmWalkText + 10, game.world.height/2, "" + cpmWalk, {font: "50px Schoolbell", fill: "#FFFFFF"});

            var shoeValueFresh = game.add.text(game.world.width/2, cpmWalkText.y + cpmWalkText.height, "Shoe Freshness: ", {font: "50px Schoolbell", fill: "#FFFFFF"});
            shoeValueFresh.anchor.setTo(1, 0);
            shoeValueFresh.x += cpmWalkText.width/2
            var shoeValue = game.add.text(shoeValueFresh + 10, game.world.height/2, "" + Math.ceil(shoeValue * freshness) + "%", {font: "50px Schoolbell", fill: "#FFFFFF"});

            var cpmSocialText = game.add.text(game.world.width/2, shoeValueFresh.y + shoeValueFresh.height, "Your CPM in Social: ", {font: "50px Schoolbell", fill: "#FFFFFF"});
            cpmSocialText.anchor.setTo(1, 0);
            cpmSocialText.x += cpmWalkText.width/2;
            var cpmSocial = game.add.text(cpmSocialText + 10, game.world.height/2, "" + cpmSocial, {font: "50px Schoolbell", fill: "#FFFFFF"});

            var followersText = game.add.text(game.world.width/2, cpmSocialText.y + cpmSocialText.height, "Followers: ", {font: "50px Schoolbell", fill: "#FFFFFF"});
            followersText.anchor.setTo(1, 0);
            followersText.x += cpmWalkText.width/2;
            var followers = game.add.text(followersText + 10, game.world.height/2, "" + followers, {font: "50px Schoolbell", fill: "#FFFFFF"});
            break;

    }
}

Pause.prototype.createShoe = function(){
    var background = game.add.tileSprite(0, 0, game.width, game.height,'bkg');
    background.alpha = 0;
    var shoe_part1;
    var shoe_part2;
    var shoe_part3;
    switch(studio_level){
        case 1:

            shoe_part1 = game.add.sprite(background.width/2-50, background.height/2-50, 'shoe1_part1');
            shoe_part1.anchor.set(0.5);
            shoe_part2 = game.add.sprite(shoe_part1.x - 182, shoe_part1.y - 40, 'shoe1_part2');
            shoe_part3 = game.add.sprite(shoe_part1.x - 220, shoe_part1.y + 30, 'shoe1_part3');

            break;

        case 2:

            shoe_part1 = game.add.sprite(background.width/2-50, background.height/2-50, 'shoe2_part1');
            shoe_part1.anchor.set(0.5);
            shoe_part2 = game.add.sprite(shoe_part1.x - 224, shoe_part1.y - 145, 'shoe2_part2');
            shoe_part3 = game.add.sprite(shoe_part1.x - 233, shoe_part1.y + 45, 'shoe2_part3');

            break;
        case 3:

            shoe_part1 = game.add.sprite(background.width/2-50, background.height/2-50, 'shoe3_part1');
            shoe_part1.anchor.set(0.5);
            shoe_part2 = game.add.sprite(shoe_part1.x - 178, shoe_part1.y - 65, 'shoe3_part2');
            shoe_part3 = game.add.sprite(shoe_part1.x - 224, shoe_part1.y + 40, 'shoe3_part3');

            break;
        case 4:
            shoe_part1 = game.add.sprite(background.width/2-50, background.height/2-50, 'shoe4_part1');
            shoe_part1.anchor.set(0.5);
            shoe_part2 = game.add.sprite(shoe_part1.x - 120, shoe_part1.y - 160, 'shoe4_part2');
            shoe_part3 = game.add.sprite(shoe_part1.x - 210, shoe_part1.y - 105, 'shoe4_part3');

            break;
    }
    shoe_part1.tint = shoe_color1;
    shoe_part2.tint = shoe_color2;
    shoe_part3.tint = shoe_color3;

    shoeGroup = game.add.group();
    shoeGroup.add(shoe_part1);
    shoeGroup.add(shoe_part2);
    shoeGroup.add(shoe_part3);
    return shoeGroup;
}

Pause.prototype.startTutorial = function(){
	// displays the tutorial window
	this.window = game.add.image(game.width/2, game.height*(2/5), 'tutorial');
	this.window.anchor.setTo(0.5, 0.5);
	this.window.bringToTop();

	// displays different text depending on state
	var tutorialString;
    var posted = this.scores[0];
    var polished = this.scores[1];

	switch (game.state.current){
		case 'Walk':
			tutorialString = "Type the words above the \n slimes to get rid of them \n or they'll lower your freshness.";
			break;
		case 'Hub':
            if (!posted && !polished){
                tutorialString = "Clean your shoes at the table, \n post about them at your computer, \n then go to sleep to start the next day.";
            } else if (!posted && polished){
                tutorialString = "Post about your shoes at \n your computer, then go to sleep\n to start the next day.";
            } else if (posted && !polished){
                tutorialString = "Clean your shoes at the table, then \ngo to sleep to start the next day.";
            } else {
                tutorialString = "Go to sleep to start the next day.";
            }
			break;
		case 'Polish':
			tutorialString = "Press the buttons on your \nkeyboard as they pass the \nbottom row to clean your shoes. \n You can get up to 150% freshness!";
			break;
		case 'StudioSelect':
			tutorialString = "Shoes are awesome! Your goal is to \n have the coolest pair around. Unlock \n each shoe store by keeping them fresh \n and posting them online! Choose a \n store to begin customizing your shoes.";
			break;
		case 'SocialMedia':
			tutorialString = "Type the red colored paragraph to get \n the word out about your new shoes. \n When you're finished, press \n enter to submit your post.";
			break;
        case 'Shoe_store_1':
            tutorialString = "Change the colors of your shoes \n to make them fit your style.";
            break;
	}
	this.tutorialText = game.add.text(this.window.width/2, this.window.height/2,
		tutorialString + "\n", {font: "42px Schoolbell", fill: "#FFFFFF", align: "center"});
	this.tutorialText.anchor.setTo(0.5);
     
}

Pause.prototype.unpause = function(){
	if (game.paused == true){

		game.paused = false;

		this.pauseText.destroy();
		this.screen.destroy();
        this.timeUnpaused = game.time.time;
        if (this.string == 'win'){
            this.win_bg.destroy();
            this.window.destroy();
            this.youWin.destroy();
            this.shoefront.destroy();
            this.shoeback.destroy();
        }
		if (this.string == 'tutorial'){
			this.window.destroy();
			this.tutorialText.destroy();
		} else if ((game.state.current == 'Walk') || (game.state.current == 'Polish')) {
			game.state.start('Hub');
		}
		game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR).onDown.remove(Pause.prototype.unpause, this);
	}
}
