function slime(text){
    this.text = text;
    this.wordSprite;
    this.style;
    this.currentChar=0;
    this.alive = true;
    this.sprite;
	this.speed = 0.05;
    
    this.initialize = function(){
		this.speed = 0.03 + Math.random()*.04;
		this.sprite = game.add.sprite(Math.random()*925, 768,'muk2');
		this.sprite.animations.add('run',[0,1,2,3,4,5,6,7,8],Math.ceil(this.speed*100)+1,true);
		this.sprite.animations.add('die',[9,10,11],6,false);
		this.sprite.play('run');
		
        this.style = { font: "28px kristen itc", fill: "#FFF", align: "center" };
        this.wordSprite = game.add.text(this.sprite.x, this.sprite.y, this.text,this.style);
        this.alive = true;
		this.wordSprite.y += 20;
		this.wordSprite.y -= 20;

    }
    
    this.move = function(px,py){
		if(this.sprite.animations.currentAnim != null && this.sprite.animations.currentAnim.isFinished)
				this.sprite.destroy();
		var distance = Math.sqrt((px-this.wordSprite.x)*(px-this.wordSprite.x)+(py-this.wordSprite.y)*(py-this.wordSprite.y));
		var normX = (px - this.wordSprite.x)/distance;
		var normY = (py - this.wordSprite.y)/distance;
        
		if(this.wordSprite.y < 600){
			this.wordSprite.y += this.speed*normY *game.time.elapsed;
			this.wordSprite.x += this.speed*normX*game.time.elapsed;;
			this.sprite.y += this.speed*normY*game.time.elapsed;;
			this.sprite.x += this.speed*normX*game.time.elapsed;;
		}else{
			this.sprite.y -= this.speed*game.time.elapsed;;
			this.wordSprite.y -= this.speed*game.time.elapsed;;
		}

		if(distance < 20 && this.alive){
			this.die();
			if(freshness > 0.06){
			freshness-=.1;
			return false;
			}
		}
		return true;
    }
    
	this.initialCheckKey = function(keycode){
        if(this.currentChar < this.text.length){
		if(this.text[this.currentChar].toUpperCase() == String.fromCharCode(keycode)){
			return true;
		}}
		return false;
	}
	
    this.checkKey = function(keycode){

        if(this.currentChar < this.text.length){
           if(this.text[this.currentChar].toUpperCase() == String.fromCharCode(keycode)){
			   totalChars++;
               this.currentChar++;
               this.wordSprite.clearColors();
               this.wordSprite.addColor('#FF0000', 0);
               this.wordSprite.addColor('#FFFFFF', this.currentChar);
           }
        }
        
        if(this.currentChar == this.text.length && this.alive == true){
            this.die();
        }
    }
    
	this.die = function(){
		this.sprite.play('die');
        this.wordSprite.destroy();

        this.alive = false;
	}
    
}
