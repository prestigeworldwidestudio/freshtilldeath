function uiBar(whereAt){
	this.where = whereAt;
    this.back;
	this.style1;
	this.followText;
	this.hintText;
	this.freshText;
	this.freshBarO;
	this.freshBar;
	this.border;
	this.hint ="";

    
    this.initialize = function(){
		this.back = game.add.image(0, 0, 'walk_white');
		this.back.width = game.world.width;
		this.back.height = 40;
		this.back.tint = 0x19195E;
		this.style1 = { font: "20px Schoolbell", fill: "#FFF", align: "center" };
        this.followText = game.add.text(10, 9, "Followers: "+followers,this.style1);
		this.hintText = game.add.text(540, 9, this.hint,this.style1);
		
		this.freshBarO = game.add.sprite(240,8,'walk_red');
		this.freshBar = game.add.sprite(240,8,'walk_green');
		this.freshBar.height = 25;
		this.freshBar.width = freshness*200;
		this.freshBarO.height = 25;
		this.freshBarO.width =200;
		
		this.freshText = game.add.text(150, 9, "Freshness:   "+freshness * 100+"%",this.style1);
		
		this.border = game.add.image(0, 38, 'walk_white');
		this.border.tint = 0x00004C;
		this.border.width = game.world.width;
		this.border.height = 5;

    }
    
    this.update = function(posted){
		if(this.where == "hub"){
			if(freshness < 1.0){
				this.hint = "Hint: Your shoes are dirty! Click the table to polish them.";
			}else if(!posted)
				this.hint = "Hint: Click on the desk to start kixstarter and get followers!";
			else if(freshness < 1.5)
				this.hint = "Hint: Polish your shoes or start a new day!";
			else
				this.hint = "Hint: Start a new day and get more followers!";
		}else if(this.where == "walk"){
			if(studio_level == 1){
				this.hint = "Hint:  Level 1 Only uses the Home-Row!";
			}else if(studio_level == 2){
				this.hint = "Hint:  Level 2 uses the home-row and the top-row!";
			}else if(studio_level == 3){
				this.hint = "Hint:  Level 3 uses the home-row and the bottom-row";
			}else if(studio_level == 4){
				this.hint = "Hint:  Level 4 uses the entire keyboard. Good Luck!";
			}

		}else if(this.where == "polish"){
			this.hint = "";
		}
		this.hintText.text = this.hint;
		this.freshText.text = "Freshness   "+Math.ceil(freshness*100)+"%";
		this.freshBar.width = freshness*200;
    }

    
}
