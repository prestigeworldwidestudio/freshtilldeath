var preloader = function(game){};

preloader.prototype = {

	preload: function(){

        // Menu
		game.load.spritesheet('button', './Images/button.png',152,40);
		game.load.image('screen', 'Images/black.png');
		game.load.image('tutorial', 'Images/tutorial.png');
        game.load.image('title', 'Images/title.png');

        // Hub
		game.load.image('bed', 'Images/bed.png');
		game.load.image('border', 'Images/border.png');
		game.load.image('carpet', 'Images/carpet.png');
		game.load.image('desk', 'Images/desk.png');
		game.load.image('full_table', 'Images/full_table.png');
		game.load.image('wall', 'Images/wall.png');
        game.load.image('checkmark', 'Images/checkmark.png');

        // Polish
        game.load.image('shine', 'Images/DDR/shine.png');
        game.load.image('shoe1_part1', 'Images/shoe1_part1.png');
        game.load.image('shoe1_part2', 'Images/shoe1_part2.png');
        game.load.image('shoe1_part3', 'Images/shoe1_part3.png');

        game.load.image('shoe2_part1', 'Images/shoe2_part1.png');
        game.load.image('shoe2_part2', 'Images/shoe2_part2.png');
        game.load.image('shoe2_part3', 'Images/shoe2_part3.png');

        game.load.image('shoe3_part1', 'Images/shoe3_part1.png');
        game.load.image('shoe3_part2', 'Images/shoe3_part2.png');
        game.load.image('shoe3_part3', 'Images/shoe3_part3.png');

        game.load.image('shoe4_part1', 'Images/shoe4_part1.png');
        game.load.image('shoe4_part2', 'Images/shoe4_part2.png');
        game.load.image('shoe4_part3', 'Images/shoe4_part3.png');

		game.load.image('semi_button', 'Images/DDR/;_buton.png')
	    game.load.image('l_button', 'Images/DDR/l_button.png');
	    game.load.image('k_button', 'Images/DDR/k_button.png');
	    game.load.image('j_button', 'Images/DDR/j_button.png');
	    game.load.image('f_button', 'Images/DDR/f_button.png');
	    game.load.image('d_button', 'Images/DDR/d_button.png');
	    game.load.image('s_button', 'Images/DDR/s_button.png');
	    game.load.image('a_button', 'Images/DDR/a_button.png');

	    game.load.image('bkg', 'Images/carpet.png');
	    game.load.image('stage', 'Images/DDR/table_stage.png');
	    game.load.image('fresh_meter', 'Images/DDR/fresh_meter.png');
        
	    game.load.image('polish_red', 'Images/DDR/red.png');
	    game.load.image('polish_purple', 'Images/DDR/purple.png');
	    game.load.image('polish_green', 'Images/DDR/green.png');
	    game.load.image('polish_blue', 'Images/DDR/blue.png');

	    game.load.image('columns', 'Images/DDR/columns.png');
	    game.load.image('button_particle', 'Images/DDR/button_particle.png');
       


        // Shoe_store_1
        game.load.image('shoe1_part1', 'Images/shoe1_part1.png');
        game.load.image('shoe1_part2', 'Images/shoe1_part2.png');
        game.load.image('shoe1_part3', 'Images/shoe1_part3.png');

        game.load.image('shoe2_part1', 'Images/shoe2_part1.png');
        game.load.image('shoe2_part2', 'Images/shoe2_part2.png');
        game.load.image('shoe2_part3', 'Images/shoe2_part3.png');

        game.load.image('shoe3_part1', 'Images/shoe3_part1.png');
        game.load.image('shoe3_part2', 'Images/shoe3_part2.png');
        game.load.image('shoe3_part3', 'Images/shoe3_part3.png');

        game.load.image('shoe4_part1', 'Images/shoe4_part1.png');
        game.load.image('shoe4_part2', 'Images/shoe4_part2.png');
        game.load.image('shoe4_part3', 'Images/shoe4_part3.png');

		game.load.image('box', 'Images/shoe_box.png');
        game.load.image('done', 'Images/button_done.png');


		game.load.image('arrow_right', 'Images/arrow_right.png');
		game.load.image('arrow_left', 'Images/arrow_left.png');


        game.load.image('red', 'Images/white.png');
        game.load.image('blue', 'Images/white.png');
        game.load.image('yellow', 'Images/white.png');
        game.load.image('white', 'Images/white.png');
        game.load.image('green', 'Images/white.png');
        game.load.image('orange', 'Images/white.png');
        game.load.image('violet', 'Images/white.png');

		game.load.image('arrow_right', 'Images/arrow_right.png');
		game.load.image('arrow_left', 'Images/arrow_left.png');
        
        game.load.image('shoe_store_bkg', 'Images/shoe_store_bkg.png');

        

        // ShoeCreate
        game.load.image('shoeCreate_background', './Images/startWalking_bkg.png');
		game.load.spritesheet('shoeCreate_button', './Images/startWalking.png');

        // SocialMedia
		game.load.image('socialMedia_background', './Images/socialmedia.png');

        // StudioSelect
		game.load.image('shoe_store_1', 'Images/shoe_store_1.png');
		game.load.image('shoe_store_2', 'Images/shoe_store_2.png');
		game.load.image('shoe_store_3', 'Images/shoe_store_3.png');
		game.load.image('shoe_store_4', 'Images/shoe_store_4.png');
		game.load.image('shoe_store_street', 'Images/shoe_store_street.png');
        game.load.image('padlock', 'Images/padlock.png');
        game.load.image('alert', 'Images/alert.png');

        // Walk
		game.load.image('street', './Images/walking_street.png');
		game.load.spritesheet('muk2', './Images/muk2.png',167,153);
		game.load.image('shoe', './Images/shoe.png');
		game.load.image('slime','./Images/slime.png');
		game.load.image('walk_red','./Images/red.png');
		game.load.image('walk_green','./Images/green.png');
		game.load.image('walk_white','./Images/white.png');
		game.load.image('explode_particle', "./Images/explode_particle.png");
		game.load.image('difficulty_bar','./Images/difficulty_bar.png');
	},

	create: function(){
		game.state.start('Menu');
	}

}
