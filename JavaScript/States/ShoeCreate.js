var shoeCreate = function(game){
//All shoeCreate Specific Vars go here
//example
//this.text = 'hello';
    this.story_text;
    this.background;
};

shoeCreate.prototype = {

	create: function(){
        this.background = game.add.image(0,0,'shoeCreate_background');
//        this.background.scale.set(1.3);
        this.story_text = this.game.add.text(this.game.width/2-210, 180 , '', {font: '30px Schoolbell', fill: '#ffffff ', align: 'center' });
        this.story_text.text = 'Now those are some fresh kicks! \n Time to head back home to post \nabout your shoes on your blog so \nyou can get more followers! \n PS: WATCH OUT FOR THE SLIMES. \n They hate new shoes and \n will try to ruin yours!';

        // draws unpause text on screen
        var pauseText = game.add.text(game.width/2, game.height*(14/15),
                "PRESS SPACEBAR TO CONTINUE", {font: "36px Arial", fill: "#ffffff"});
        pauseText.anchor.setTo(0.5, 0.5);

        game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR).onDown.add(
            function(){
                game.input.keyboard.onDownCallback = null;
                game.state.start('Walk');
            }, this);
	}
}
