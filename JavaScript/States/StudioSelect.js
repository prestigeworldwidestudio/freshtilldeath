var studioSelect = function(game){
	var background;
	var shoeStore1;
	var shoeStore2;
	var shoeStore3;
	var shoeStore4;

    var overShoeStore1;
    var overShoeStore2;
    var overShoeStore3;
    var overShoeStore4;

    var bigShoeStore1;
    var bigShoeStore2;
    var bigShoeStore3;
    var bigShoeStore4;

    var imageName;
    var imageText;

    var padlock2;
    var padlock3;
    var padlock4;

    this.alerted = false;
};

studioSelect.prototype = {

	create: function(){
        // reset global variables
        studioSelect.prototype.resetGlobals();

        // initializing variables
        overShoeStore1 = false;
        overShoeStore2 = false;
        overShoeStore3 = false;
        overShoeStore4 = false;
        imageName = "";
        imageText = this.game.add.text(0, 0,
			"", {font: "50px Arial", fill: "#000000"});

		// creating and scaling background
		background = this.game.add.image(0, 0, 'shoe_store_street');
		background.scale.setTo(0.75);

		// creating, anchoring, and scaling shoe stores
		shoeStore1 = this.game.add.sprite(this.game.width*(1/15), this.game.height*(3/5), 'shoe_store_1');
		shoeStore2 = this.game.add.sprite(this.game.width*(4/15), this.game.height*(3/5), 'shoe_store_2');
		shoeStore3 = this.game.add.sprite(this.game.width*(7/15), this.game.height*(3/5), 'shoe_store_3');
		shoeStore4 = this.game.add.sprite(this.game.width*(11/15), this.game.height*(3/5), 'shoe_store_4');

		shoeStore1.anchor.setTo(0, 1);
		shoeStore2.anchor.setTo(0, 1);
		shoeStore3.anchor.setTo(0, 1);
		shoeStore4.anchor.setTo(0, 1);

		shoeStore1.scale.setTo(0.25);
		shoeStore2.scale.setTo(0.25);
		shoeStore3.scale.setTo(0.25);
		shoeStore4.scale.setTo(0.25);

        bigShoeStore1 = this.game.add.image(shoeStore1.x + shoeStore1.width/2, shoeStore1.y - shoeStore1.height/2, 'shoe_store_1');
        bigShoeStore2 = this.game.add.image(shoeStore2.x + shoeStore2.width/2, shoeStore2.y - shoeStore2.height/2, 'shoe_store_2');
        bigShoeStore3 = this.game.add.image(shoeStore3.x + shoeStore3.width/2, shoeStore3.y - shoeStore3.height/2, 'shoe_store_3');
        bigShoeStore4 = this.game.add.image(shoeStore4.x + shoeStore4.width/2, shoeStore4.y - shoeStore4.height/2, 'shoe_store_4');

        bigShoeStore1.scale.setTo(0.27);
        bigShoeStore1.anchor.setTo(0.5, 0.5);
        bigShoeStore2.scale.setTo(0.27);
        bigShoeStore2.anchor.setTo(0.5, 0.5);
        bigShoeStore3.scale.setTo(0.27);
        bigShoeStore3.anchor.setTo(0.5, 0.5);
        bigShoeStore4.scale.setTo(0.27);
        bigShoeStore4.anchor.setTo(0.5, 0.5);

        bigShoeStore1.alpha = 0;
        bigShoeStore2.alpha = 0;
        bigShoeStore3.alpha = 0;
        bigShoeStore4.alpha = 0;

		// enabling clicking on sprites
		shoeStore1.inputEnabled = true;
		shoeStore2.inputEnabled = true;
		shoeStore3.inputEnabled = true;
		shoeStore4.inputEnabled = true;

        // uses hand cursor when over these images
        shoeStore1.input.useHandCursor = true;
        shoeStore2.input.useHandCursor = true;
        shoeStore3.input.useHandCursor = true;
        shoeStore4.input.useHandCursor = true;

		// setting callback for clicking on sprites
		shoeStore1.events.onInputDown.add(this.onDown);
		shoeStore2.events.onInputDown.add(this.onDown);
		shoeStore3.events.onInputDown.add(this.onDown);
		shoeStore4.events.onInputDown.add(this.onDown);

        // expands studios on hover
        shoeStore1.events.onInputOver.add(this.hovering);
        shoeStore2.events.onInputOver.add(this.hovering);
        shoeStore3.events.onInputOver.add(this.hovering);
        shoeStore4.events.onInputOver.add(this.hovering);
        shoeStore1.events.onInputOut.add(this.notHovering);
        shoeStore2.events.onInputOut.add(this.notHovering);
        shoeStore3.events.onInputOut.add(this.notHovering);
        shoeStore4.events.onInputOut.add(this.notHovering);

        padlock2 = this.game.add.image(0, 0, 'padlock');
        padlock3 = this.game.add.image(0, 0, 'padlock');
        padlock4 = this.game.add.image(0, 0, 'padlock');
        padlock2.scale.setTo(0.2);
        padlock3.scale.setTo(0.2);
        padlock4.scale.setTo(0.2);
        padlock2.x = shoeStore2.x + shoeStore2.width/2 - padlock2.width/2;
        padlock2.y = shoeStore2.y - shoeStore2.height/2 - padlock2.height/2;
        padlock3.x = shoeStore3.x + shoeStore3.width/2 - padlock2.width/2;
        padlock3.y = shoeStore3.y - shoeStore3.height/2 - padlock2.height/2;
        padlock4.x = shoeStore4.x + shoeStore4.width/2 - padlock2.width/2;
        padlock4.y = shoeStore4.y - shoeStore4.height/2 - padlock2.height/2;
        padlock2.alpha = 0;
        padlock3.alpha = 0;
        padlock4.alpha = 0;

        var displayedFollowers = this.game.add.text(0, 0, "Followers: " + followers, {font: "40px Arial", fill: "#FFFFFF"});
        displayedFollowers.x = this.game.world.width/2;
        displayedFollowers.y = this.game.world.height*(14/15);
        displayedFollowers.anchor.setTo(0.5, 1);

        this.checkIfEnoughFollowers();
        this.showFollowerReqs();

		if (tutorialsOn) { var tutorial = new Pause('tutorial', {}); }
        if (won) { var winningScreen = new Pause('win'); }
	},

    update: function(){
        /*if (overShoeStore1){
            this.keepWordsOverCursor(shoeStore1);
        } else if (overShoeStore2){
            this.keepWordsOverCursor(shoeStore2);
        } else if (overShoeStore3){
            this.keepWordsOverCursor(shoeStore3);
        } else if (overShoeStore4){
            this.keepWordsOverCursor(shoeStore4);
        }*/
    },

    checkIfEnoughFollowers: function(){
        if (followers >= followersReq2){
            store2locked = false;
        } else {
            shoeStore2.tint = "0x525252";
            padlock2.alpha = 1;
        }
        if (followers >= followersReq3){
            store3locked = false;
        } else {
            shoeStore3.tint = "0x525252";
            padlock3.alpha = 1;
        }
        if (followers >= followersReq4){
            store4locked = false;
        } else {
            shoeStore4.tint = "0x525252";
            padlock4.alpha = 1;
        }
    },

    showFollowerReqs: function(){
        for (i = 0; i < 3; i++){
            if ((i == 0 && store2locked == true) || (i == 1 && store3locked == true) || (i == 2 && store4locked == true)){
                var shoeStore;
                var followersReq;
                if (i == 0){
                    shoeStore = shoeStore2;
                    followersReq = followersReq2;
                } else if (i == 1){
                    shoeStore = shoeStore3;
                    followersReq = followersReq3;
                } else if (i == 2){
                    shoeStore = shoeStore4;
                    followersReq = followersReq4;
                }

                // creates a graphics object and draws/fills a circle
                var cirlceRadius = 40;
                var graphics = this.game.add.graphics(0, 0);
                graphics.lineStyle(0);
                graphics.beginFill(0x000000, 0.7);
                graphics.drawCircle(0, 0, cirlceRadius);
                graphics.endFill();
                graphics.x = shoeStore.x + shoeStore.width*(1/3);
                graphics.y = shoeStore.y + cirlceRadius;

                // draws an F in the circle
                var fInCircle = this.game.add.text(graphics.x, graphics.y, "F", {font: "30px Arial", fill: "#FFFFFF"});
                fInCircle.x -= fInCircle.width/2;
                fInCircle.y -= fInCircle.height/2 - 4;

                // draws numerical follower req
                var store2Reqs = this.game.add.text(graphics.x, graphics.y, followersReq, {font: "30px Arial", fill: "#FFFFFF"});
                store2Reqs.x = fInCircle.x + fInCircle.width*2;
                store2Reqs.y = fInCircle.y;
            }
        }
    },

    showAlert: function(){
        if (this.alerted != true){
            var alertWindow = game.add.image(0, 0, 'alert');
            var alertText = game.add.text(game.width/2, game.height*(2/10) + 15, "You need more followers before going in this store.\n", {font: "30px Schoolbell", fill: "#FFFFFF"});
            alertText.anchor.setTo(0.5, 0.5);
            var timer = game.time.create();
            this.alerted = true;
            timer.add(3000, function(){
                this.alerted = false;
                alertWindow.destroy();
                alertText.destroy();
            }, this);
            timer.start();
        }
    },

	onDown: function(sprite){
		switch (sprite.key){
			case 'shoe_store_1':
				this.game.state.start('Shoe_store_1');
				studio_level = 1;
				break;
			case 'shoe_store_2':
                if (store2locked == false){
                    this.game.state.start('Shoe_store_1');
			        studio_level = 2;
                } else {
                    studioSelect.prototype.showAlert();
                }
				break;
			case 'shoe_store_3':
                if (store3locked == false){
                    this.game.state.start('Shoe_store_1');
			        studio_level =3;
                } else {
                    studioSelect.prototype.showAlert();
                }
				break;
			case 'shoe_store_4':
                if (store4locked == false){
                    studio_level = 4;
			        this.game.state.start('Shoe_store_1');
                } else {
                    studioSelect.prototype.showAlert();
                }
				break;
		}
	},

    notHovering: function(image){
        imageText.text = "";
        switch (image.key){
            case 'shoe_store_1':
                overShoeStore1 = false;
                bigShoeStore1.alpha = 0;
                shoeStore1.alpha = 1;
                break;
            case 'shoe_store_2':
                overShoeStore2 = false;
                bigShoeStore2.alpha = 0;
                shoeStore2.alpha = 1;
                break;
            case 'shoe_store_3':
                overShoeStore3 = false;
                bigShoeStore3.alpha = 0;
                shoeStore3.alpha = 1;
                break;
            case 'shoe_store_4':
                overShoeStore4 = false;
                bigShoeStore4.alpha = 0;
                shoeStore4.alpha = 1;
                break;
        }
    },

	keepWordsOverCursor: function(image){
		imageText.x = this.game.input.mousePointer.x - imageText.width/2;
		imageText.y = this.game.input.mousePointer.y;
		imageText.anchor.setTo(0, 1);
	},

    hovering: function(image){
        switch (image.key){
            case 'shoe_store_1':
                imageName = "SHOE STORE ";
                overShoeStore1 = true;
                bigShoeStore1.alpha = 1;
                shoeStore1.alpha = 0;
                break;
            case 'shoe_store_2':
                if (store2locked == false){
                    imageName = "SHOE STORE ";
                    overShoeStore2 = true;
                    bigShoeStore2.alpha = 1;
                    shoeStore2.alpha = 0;
                }
                break;
            case 'shoe_store_3':
                if (store3locked == false){
                    imageName = "SHOE STORE ";
                    overShoeStore3 = true;
                    bigShoeStore3.alpha = 1;
                    shoeStore3.alpha = 0;
                }
                break;
            case 'shoe_store_4':
                if (store4locked == false){
                    imageName = "SHOE STORE ";
                    overShoeStore4 = true;
                    bigShoeStore4.alpha = 1;
                    shoeStore4.alpha = 0;
                }
                break;
        }
		/*imageText = this.game.add.text(image.x, image.y,
		imageName, {font: "50px Arial", fill: "#ffffff"});*/
		imageText.stroke = 'rgba(0, 0, 0, 1)';
		imageText.strokeThickness = 8;
    },

    resetGlobals: function(){
//        shoe_color1 = 0xffffff;
//        shoe_color2 = 0xffffff;
//        shoe_color3 = 0xffffff;

        cpmWalk = 0;
        cpmSocial = 0;
        freshness = 1;

        posted = false;
        polished = false;
    }

}
