
var shoe_1 = function (game) {
    this.shoe1_part1;
    this.shoe1_part2;
    this.shoe1_part3;

    this.shoe2_part1;
    this.shoe2_part2;
    this.shoe2_part3;

	this.shoe3_part1;
    this.shoe3_part2;
    this.shoe3_part3;

	this.shoe4_part1;
    this.shoe4_part2;
    this.shoe4_part3;

	this.color_array;
	
    this.selected;

    this.red;
    this.blue;
    this.yellow;
    this.white;
    this.green;
    this.orange;
    this.violet;

    this.color;
    this.selected_color;

	this.box;
	this.done;

	this.arrow_right_bottom;
	this.arrow_left_bottom;
	this.arrow_color_bottom;

	this.arrow_right_middle;
	this.arrow_left_middle;
	this.arrow_color_middle;

	this.arrow_right_top;
	this.arrow_left_top;
	this.arrow_color_top;

	
	this.carpet;
	this.wall;
	
    this.currentArrowRow;

    this.tutorial;
};

shoe_1.prototype = {

    preload: function () {

    },

    create: function () {
        this.currentArrowRow = 'top';

	color_array=["0xed1c24","0x464646","0x0054a6","0xfff200","0x00a651","0xf7941d","0x662d91","0xffffff"];
	arrow_color_bottom=-1;
	arrow_color_middle=-1;
	arrow_color_top=-1;

     
	 
		wall=game.add.image(0,0,'shoeCreate_background');
	 
		carpet=game.add.image(0, 450, 'bkg');
		carpet.scale.setTo(3);
		


        background = this.game.add.image(0,0,'shoe_store_bkg');
        background.scale.setTo(1.4);

	    box = game.add.image(game.world.centerX-20, game.world.centerY+200, 'box');
		box.scale.setTo(.7);
		box.x = box.x - box.width / 2;
		box.y = box.y - box.height / 2;

		arrow_right_bottom = game.add.button(game.world.centerX+330, game.world.centerY+70, 'arrow_right',this.arrow_right_bottom,this);
		arrow_left_bottom = game.add.button(game.world.centerX-430, game.world.centerY+70, 'arrow_left',this.arrow_left_bottom,this);


		arrow_right_middle = game.add.button(game.world.centerX+330, game.world.centerY+10, 'arrow_right',this.arrow_right_middle,this);
		arrow_left_middle = game.add.button(game.world.centerX-430, game.world.centerY+10, 'arrow_left',this.arrow_left_middle,this);

		arrow_right_top = game.add.button(game.world.centerX+330, game.world.centerY-50, 'arrow_right',this.arrow_right_top,this);
		arrow_left_top = game.add.button(game.world.centerX-430, game.world.centerY-50, 'arrow_left',this.arrow_left_top,this);


		if(studio_level==1){
			console.log("inside first if");
        shoe1_part1 = game.add.button(game.world.centerX, game.world.centerY, 'shoe1_part1', this.changeShoeColor, this);
		shoe1_part1.anchor.set(0.5);


        shoe1_part2 = game.add.button(shoe1_part1.x-182, shoe1_part1.y - 40, 'shoe1_part2', this.changeWaveColor, this);


		shoe1_part3 = game.add.button(shoe1_part1.x - 220, shoe1_part1.y + 30, 'shoe1_part3', this.changeBottomColor, this);

            shoe1_part1.tint = shoe_color1;
            shoe1_part2.tint = shoe_color2;
            shoe1_part3.tint = shoe_color3;

		
      
		}else
			if(studio_level==2){
				console.log("Inside second if");
				shoe2_part1 = game.add.button(game.world.centerX, game.world.centerY, 'shoe2_part1', this.changeShoeColor, this);
				shoe2_part1.anchor.set(0.5);







				shoe2_part2 = game.add.button(shoe2_part1.x - 224, shoe2_part1.y - 145, 'shoe2_part2', this.changeWaveColor, this);



				shoe2_part3 = game.add.button(shoe2_part1.x - 233, shoe2_part1.y + 45, 'shoe2_part3', this.changeBottomColor, this);


				shoe2_part1.tint = shoe_color1;
                shoe2_part2.tint = shoe_color2;
                shoe2_part3.tint = shoe_color3;
			


			}else

				if(studio_level==3){

					 shoe3_part1 = game.add.button(game.world.centerX, game.world.centerY, 'shoe3_part1', this.changeShoeColor, this);
						shoe3_part1.anchor.set(0.5);

						shoe3_part2 = game.add.button(shoe3_part1.x - 178, shoe3_part1.y - 65, 'shoe3_part2', this.changeWaveColor, this);

						shoe3_part3 = game.add.button(shoe3_part1.x - 224, shoe3_part1.y + 40, 'shoe3_part3', this.changeBottomColor, this);

                shoe3_part1.tint = shoe_color1;
                shoe3_part2.tint = shoe_color2;
                shoe3_part3.tint = shoe_color3;
                    
				}else

				{
					shoe4_part1 = game.add.button(game.world.centerX, game.world.centerY, 'shoe4_part1', this.changeShoeColor, this);
					shoe4_part1.anchor.set(0.5);

					shoe4_part2 = game.add.button(shoe4_part1.x - 120, shoe4_part1.y - 160, 'shoe4_part2', this.changeWaveColor, this);

					shoe4_part3 = game.add.button(shoe4_part1.x -210, shoe4_part1.y -105, 'shoe4_part3', this.changeBottomColor, this);

                    shoe4_part1.tint = shoe_color1;
                    shoe4_part2.tint = shoe_color2;
                    shoe4_part3.tint = shoe_color3;

				}
        arrow_left_top.tint = "0x24cc2a";
        arrow_right_top.tint = "0x24cc2a";

        game.input.keyboard.callbackContext = this;
		game.input.keyboard.onDownCallback = this.checkKeyboardInput;

        /*// creates a graphics object and draws/fills a circle
        var graphics = this.game.add.graphics(0, 0);
        graphics.lineStyle(0);
        graphics.beginFill(0x000000, 0.9);
        graphics.drawEllipse(0, 0, 200, 200);
        graphics.endFill();*/

        // draws unpause text on screen
        var pauseText = game.add.text(game.width/2, game.height*(14/15),
                "PRESS SPACEBAR TO CONTINUE", {font: "36px Arial", fill: "#000000"});
        pauseText.anchor.setTo(0.5, 0.5);

        //graphics.x = pauseText.x + pauseText.width*(1/3);
        //graphics.y = pauseText.y;

        if (tutorialsOn) { this.tutorial = new Pause('tutorial', {}); }
    },

    checkKeyboardInput: function(){
        switch (game.input.keyboard.lastKey.keyCode){
            case Phaser.Keyboard.UP:
                switch (this.currentArrowRow){
                    case 'middle':
                        this.switchArrowColors('top');
                        break;
                    case 'bottom':
                        this.switchArrowColors('middle');
                        break;
                }
                break;
            case Phaser.Keyboard.DOWN:
                switch (this.currentArrowRow){
                    case 'middle':
                        this.switchArrowColors('bottom');
                        break;
                    case 'top':
                        this.switchArrowColors('middle');
                        break;
                }
                break;
            case Phaser.Keyboard.LEFT:
                switch (this.currentArrowRow){
                    case 'top':
                        this.arrow_left_top();
                        break;
                    case 'middle':
                        this.arrow_left_middle();
                        break;
                    case 'bottom':
                        this.arrow_left_bottom();
                        break;
                }
                break;
            case Phaser.Keyboard.RIGHT:
                switch (this.currentArrowRow){
                    case 'top':
                        this.arrow_right_top();
                        break;
                    case 'middle':
                        this.arrow_right_middle();
                        break;
                    case 'bottom':
                        this.arrow_right_bottom();
                        break;
                }
                break;
            case Phaser.Keyboard.SPACEBAR:
                if (game.time.time >= this.tutorial.timeUnpaused + 100){
                    game.input.keyboard.onDownCallback = null;
                    this.game.state.start('ShoeCreate');
                }
                break;
        }
    },

    arrow_right_bottom: function () {
        this.switchArrowColors('bottom');
        arrow_color_bottom=arrow_color_bottom+1;
		console.log("bottom right numbers:"+arrow_color_bottom);

		if(arrow_color_bottom<0){
			arrow_color_bottom=arrow_color_bottom*-1;
			console.log("bottom converted number:"+arrow_color_bottom);
		}

		color=color_array[(arrow_color_bottom)%8];
		this.changeBottomColor();
    },

	arrow_left_bottom: function () {
        this.switchArrowColors('bottom');
        arrow_color_bottom=arrow_color_bottom-1;
		console.log("left numbers:"+arrow_color_bottom);
		if(arrow_color_bottom>-1){
		color=color_array[(arrow_color_bottom)%8];
		this.changeBottomColor();
		}
    },

	arrow_right_middle: function () {
        this.switchArrowColors('middle');
        arrow_color_middle=arrow_color_middle+1;
		console.log("middle right numbers:"+arrow_color_middle);

		if(arrow_color_middle<0){
			arrow_color_middle=arrow_color_middle*-1;
			console.log("middle converted number:"+arrow_color_middle);
		}

		color=color_array[(arrow_color_middle)%8];
		this.changeWaveColor();
    },

	arrow_left_middle: function () {
        this.switchArrowColors('middle');
        arrow_color_middle=arrow_color_middle-1;
		console.log("middle left numbers:"+arrow_color_middle);
		if(arrow_color_middle>-1){
		color=color_array[(arrow_color_middle)%8];
		this.changeWaveColor();
		}
    },

	arrow_right_top: function () {
        this.switchArrowColors('top');
        arrow_color_top=arrow_color_top+1;
		console.log("top right numbers:"+arrow_color_top);

		if(arrow_color_top<0){
			arrow_color_top=arrow_color_top*-1;
			console.log("top converted number:"+arrow_color_top);
		}

		color=color_array[(arrow_color_top)%8];
		this.changeShoeColor();
    },

	arrow_left_top: function () {
        this.switchArrowColors('top');
        arrow_color_top=arrow_color_top-1;
		console.log("left numbers:"+arrow_color_top);
		if(arrow_color_top>-1){
		color=color_array[(arrow_color_top)%8];
		this.changeShoeColor();
		}
    },

    switchArrowColors: function(row){
        switch (row){
            case 'top':
                this.currentArrowRow = 'top';
                arrow_left_top.tint = "0x24cc2a";
                arrow_right_top.tint = "0x24cc2a";

                arrow_left_middle.tint = "0xFFFFFF";
                arrow_right_middle.tint = "0xFFFFFF";
                arrow_left_bottom.tint = "0xFFFFFF";
                arrow_right_bottom.tint = "0xFFFFFF";
                break;
            case 'middle':
                this.currentArrowRow = 'middle';
                arrow_left_middle.tint = "0x24cc2a";
                arrow_right_middle.tint = "0x24cc2a";

                arrow_left_top.tint = "0xFFFFFF";
                arrow_right_top.tint = "0xFFFFFF";
                arrow_left_bottom.tint = "0xFFFFFF";
                arrow_right_bottom.tint = "0xFFFFFF";
                break;
            case 'bottom':
                this.currentArrowRow = 'bottom';
                arrow_left_bottom.tint = "0x24cc2a";
                arrow_right_bottom.tint = "0x24cc2a";

                arrow_left_top.tint = "0xFFFFFF";
                arrow_right_top.tint = "0xFFFFFF";
                arrow_left_middle.tint = "0xFFFFFF";
                arrow_right_middle.tint = "0xFFFFFF";
                break;
        }
    },

    changeShoeColor: function () {
		shoe_color1=color;
        if(studio_level==1){
		shoe1_part1.tint = color;
		}else
			if(studio_level==2){
				shoe2_part2.tint=color;
				console.log("it selected the color!")
			}else
				if(studio_level==3){
					shoe3_part1.tint=color;
				}else{
					shoe4_part1.tint=color;
				}

    },

    changeWaveColor: function () {
		console.log("It's in the wave function!");
       shoe_color2=color;

		if(studio_level==1){
		shoe1_part2.tint = color;
		}else
			if(studio_level==2){
				shoe2_part1.tint=color;
				console.log("it selected the color!")
			}else
				if(studio_level==3){
					shoe3_part2.tint=color;
				}else{
					shoe4_part2.tint=color;
				}


    },

    changeBottomColor: function () {


		shoe_color3=color;

		if(studio_level==1){
		shoe1_part3.tint = color;
		}else
			if(studio_level==2){
				shoe2_part3.tint=color;
				console.log("it selected the color!")
			}else
				if(studio_level==3){
					shoe3_part3.tint=color;
				}else{
					shoe4_part3.tint=color;
				}
    }



}
