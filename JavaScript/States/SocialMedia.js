var socialMedia = function(game){
	this.curPos;
	this.black;
	this.cursorTime;
	this.lastCursorTime;
	this.userText = "";
	this.style = { font: "40px Schoolbell", fill: "#FF0000", align: "left", wordWrap: true, wordWrapWidth: 400};
	this.style2 = { font: "40px Schoolbell", fill: "#33CC33", align: "left", wordWrap: true, wordWrapWidth: 400};
	this.text;
	this.text2;
	this.bool = false;
	this.testKey="";
	this.charCount=0;
	this.promptText;
	this.promptText1;
	this.promptText2;
	this.promptText3;
	this.promptText4;
	this.lastKey;
	this.key_shift;
	this.showPrompt=true;
	this.showUser=true;
	this.showFinish;
	this.finishTest="KixStarter Post accepted!";
	this.firstTime;
	this.keepGoing;
	this.i=0;
	this.startTime;
	this.elapsedTime;
	this.firstKeyPress;
	this.cpm;
	this.date;
	this.promptChar;
	this.done;
	this.stringsMatch;
	this.shoeGroup;
	this.tempString;
	this.stringVar;
	this.timer;
//All socialMedia Specific Vars go here
//example
//this.text = 'hello';
};

socialMedia.prototype = {

	create: function(){
		socialMedia.curPos=0;
		socialMedia.black=true;
		socialMedia.date = new Date();
		socialMedia.lastCursorTime=socialMedia.date.getTime();
		socialMedia.timer = 0;
		socialMedia.stringVar = Math.floor(Math.random() * (1 - 0 + 1)) + 0;
		socialMedia.tempString="";
		socialMedia.stringsMatch=false;
		socialMedia.done = false;
		socialMedia.firstKeyPress = true;
		socialMedia.startTime=0;
		socialMedia.stopTime=0;
		socialMedia.elapsedTime=0;
		socialMedia.cpm=0;
		game.add.image(0,0,'socialMedia_background');
		socialMedia.firstTime=false;
		socialMedia.i=0;
		socialMedia.finishText="KixStarter Post accepted!";
		socialMedia.showUser = true;
		socialMedia.showFinish = false;
				
		switch(studio_level){
			case 1:
				if(socialMedia.stringVar.toString() == "1"){
					socialMedia.promptText = "|Just got sponsored by Ma and Pa's Shoe Shack. Be sure to check out their store for some cool hand made shoes at a good price.";
				}
				else{
					socialMedia.promptText = "|These new shoes are awesome. Too bad those gross slimes keep trying to dirty them. Seriously, I'm trying to keep my swag up.";
				}
				break;
			case 2:
				if(socialMedia.stringVar.toString() == "1"){
					socialMedia.promptText = "|Just picked up some fresh sneakers. I love customizing the colors on my favorite brands. The new store that just opened has all the latest stuff.";
				}
				else{
					socialMedia.promptText = "|Okay everybody, I've got some good news for you. Next time you go to Sweet Soles on Third Street mention me for a discount on your next pair.";
				}
				break;
			case 3:
				if(socialMedia.stringVar.toString() == "1"){
					socialMedia.promptText = "|Hey Kix collectors, if you are looking for some new shoes check out these sweet kicks. For only twenty bucks you can get your hands on these. Don't forget to follow me here and like my new shoes.";
				}
				else{
				}
				socialMedia.promptText = "|If you are looking to get some new gear there is a great new store nearby. They have a huge selection of shoes just like these. Don't forget to add your very own personal touch.";
				break;
			case 4:
				if(socialMedia.stringVar.toString() == "1"){
					socialMedia.promptText = "|For anyone who loves shoes you have to get these if you value your feet. If you want to jump higher and run faster you need to get these. Message me for details on where to get yourself a pair on sale.";
				}
				else{
					socialMedia.promptText = "|Check out what I just scored. These shoes just came out today and luckily I got my hands on a pair. Threw some custom colors on them and made them my own. By far the best purchase of the year.";
				}
				break;
		}
				
		socialMedia.promptChar = 100;
		socialMedia.testKey = "";
		socialMedia.text=game.add.text(640, 650, "Followers: " + followers, this.style2);
		socialMedia.text3=game.add.text(100, 650, "Press Enter to submit", this.style2);
		game.input.keyboard.callbackContext = this;
		game.input.keyboard.onDownCallback = this.checkKeyboardInput;
		socialMedia.text2=game.add.text(80, 210, socialMedia.promptText + "\n", this.style);
		socialMedia.key_shift = game.input.keyboard.addKey(Phaser.Keyboard.SHIFT);
		for(var i = 0; i < socialMedia.promptText.length-1;i++){
			if(socialMedia.promptText.charAt(i).toString() == '|'){
			socialMedia.text2.addColor('#000000',i);	
			}
		}
		
		this.createShoe();
		if (tutorialsOn) { var tutorial = new Pause('tutorial', {}); }
	},
	
	createShoe: function(){
        var shoe_part1;
        var shoe_part2;
        var shoe_part3;
			switch(studio_level){
                case 1:
                    shoe_part1 = game.add.sprite(background.width/2+210, background.height/2-10, 'shoe1_part1');
                    shoe_part1.anchor.set(0.5);
                    shoe_part2 = game.add.sprite(shoe_part1.x - 136.5, shoe_part1.y - 30, 'shoe1_part2');
                    shoe_part3 = game.add.sprite(shoe_part1.x - 165, shoe_part1.y + 22.5, 'shoe1_part3');

                    break;

                case 2:
                    shoe_part1 = game.add.sprite(background.width/2+210, background.height/2-10, 'shoe2_part1');
                    shoe_part1.anchor.set(0.5);
                    shoe_part2 = game.add.sprite(shoe_part1.x - 168, shoe_part1.y - 108.75, 'shoe2_part2');
                    shoe_part3 = game.add.sprite(shoe_part1.x - 174.75, shoe_part1.y + 33.75, 'shoe2_part3');

                    break;
                case 3:
                    shoe_part1 = game.add.sprite(background.width/2+210, background.height/2-10, 'shoe3_part1');
                    shoe_part1.anchor.set(0.5);
                    shoe_part2 = game.add.sprite(shoe_part1.x - 133.5, shoe_part1.y - 48.75, 'shoe3_part2');
                    shoe_part3 = game.add.sprite(shoe_part1.x - 168, shoe_part1.y + 30, 'shoe3_part3');

                    break;
                case 4:
                    shoe_part1 = game.add.sprite(background.width/2+250, background.height/2-50, 'shoe4_part1');
                    shoe_part1.anchor.set(0.5);
                    shoe_part2 = game.add.sprite(shoe_part1.x - 90, shoe_part1.y - 120, 'shoe4_part2');
                    shoe_part3 = game.add.sprite(shoe_part1.x - 157.5, shoe_part1.y - 78.75, 'shoe4_part3');

                    break;
			}
            shoe_part1.tint = shoe_color1;
            shoe_part2.tint = shoe_color2;
            shoe_part3.tint = shoe_color3;
			
			shoe_part1.scale.x = 0.75;
            shoe_part2.scale.x = 0.75;
            shoe_part3.scale.x = 0.75;
			shoe_part1.scale.y = 0.75;
            shoe_part2.scale.y = 0.75;
            shoe_part3.scale.y = 0.75;
			
            socialMedia.shoeGroup = game.add.group();
            socialMedia.shoeGroup.add(shoe_part1);
            socialMedia.shoeGroup.add(shoe_part2);
            socialMedia.shoeGroup.add(shoe_part3);
		},

	update: function(){
		this.checkText();
	},

	//________________________________________________________________________

	submitText: function(){
		if(socialMedia.userText.length==socialMedia.promptText.length-1 && !socialMedia.done){
			socialMedia.elapsedTime = socialMedia.stopTime-socialMedia.startTime;
			socialMedia.cpm = Math.floor(socialMedia.promptChar/((socialMedia.elapsedTime)/60000));
			socialMedia.finishText="KixStarter Post accepted! Your cpm: " + socialMedia.cpm.toString();
			cpmSocial = socialMedia.cpm;
			socialMedia.showUser=false;
			socialMedia.text2.destroy();
			socialMedia.showFinish=true;
			socialMedia.done=true;
			posted = true;
			var scores = [];
            scores[0] = followers;
            scores[1] = cpmSocial;
            scores[2] = cpmWalk;
            scores[3] = shoeValue;
            scores[4] = freshness;
			var pause = new Pause('score', scores);
			socialMedia.firstKeyPress = true;
			socialMedia.stopTime=0;
			socialMedia.startTime=0;
			socialMedia.userText="";
			socialMedia.showFinish=false;
			socialMedia.showUser=true;
			socialMedia.cpm=0;
			socialMedia.firstKeyPress = true;
			game.state.start('Hub');
		}
	},
	
	incrementTimer: function(){
		
	},

	checkText: function(){
	
		if(socialMedia.userText!=null){
			if(socialMedia.userText.length == socialMedia.promptText.length-1){
				if(!socialMedia.stringsMatch){
					socialMedia.date = new Date();
					socialMedia.stopTime = socialMedia.date.getTime();
					socialMedia.stringsMatch=true;
				}
			}
			socialMedia.text2.destroy();
			socialMedia.text2=game.add.text(80, 210, socialMedia.promptText + "\n", this.style);
		}
		
		socialMedia.text2.addColor('#000000',0);
		socialMedia.keepGoing=true;		
		if(socialMedia.userText!=null){
			for(var i = 0; i<socialMedia.userText.length; i++){
				if(socialMedia.keepGoing){
					if(String(socialMedia.userText.charAt(i))==String(socialMedia.promptText.charAt(i))){
						socialMedia.text2.addColor('#33CC33',i);
						socialMedia.text2.addColor('#FF0000',i+1);
						socialMedia.curPos=i+1;
					}
					else{
						socialMedia.keepGoing=false;
					}
				}
			}			
			socialMedia.text2.addColor('#000000',socialMedia.curPos);
			socialMedia.text2.addColor('#FF0000',socialMedia.curPos+1);		
		}
	},


	checkKey: function(){

		if(game.input.keyboard.lastKey!=null){
			if(socialMedia.key_shift.isDown){
				if(game.input.keyboard.lastKey.keyCode==49){
					socialMedia.testKey = "!";
				}
				else{
					socialMedia.testKey = String.fromCharCode(
								game.input.keyboard.lastKey.keyCode).toUpperCase();
				}
			}
			else if(game.input.keyboard.lastKey.keyCode==16){
			}
			else if(game.input.keyboard.lastKey.keyCode==13){
				this.submitText();
			}
			/*else if(game.input.keyboard.lastKey.keyCode==27){
				socialMedia.firstKeyPress = true;
				socialMedia.stopTime=0;
				socialMedia.startTime=0;
				socialMedia.userText="";
				socialMedia.showFinish=false;
				socialMedia.showUser=true;
				socialMedia.cpm=0;
				socialMedia.firstKeyPress = true;
				game.state.start('Hub');
			}
			*/
			else{
				if(game.input.keyboard.lastKey.keyCode==190){
					socialMedia.testKey = ".";
				}
				else if(game.input.keyboard.lastKey.keyCode==188){
					socialMedia.testKey = ",";
				}
				else if(game.input.keyboard.lastKey.keyCode==222){
					socialMedia.testKey = "'";
				}
				else{
					socialMedia.testKey = String.fromCharCode(
								game.input.keyboard.lastKey.keyCode).toLowerCase();
				}
			}
			socialMedia.lastKey = game.input.keyboard.lastKey.keyCode;
		}

		if(socialMedia.promptText.charAt(socialMedia.i+1)==socialMedia.testKey.charAt(0)){
			if(socialMedia.firstKeyPress){
				socialMedia.firstKeyPress = false;
				socialMedia.date = new Date();
				socialMedia.startTime=socialMedia.date.getTime();
			}
			socialMedia.userText += socialMedia.testKey;
			socialMedia.promptText = socialMedia.promptText.replace('|','');
			socialMedia.i++;
			socialMedia.promptText = [socialMedia.promptText.slice(0, socialMedia.i), "|", socialMedia.promptText.slice(socialMedia.i)].join('');
		}
	},


	checkKeyboardInput: function(){
		if(socialMedia.userText==null){
			socialMedia.userText="";
		}

		if(game.input.keyboard.lastKey!=null){
			this.checkKey();
		}
	}
}
