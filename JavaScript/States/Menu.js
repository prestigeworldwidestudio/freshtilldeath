//GLOBAL VARIABLES
var studio_level = 1;
var followers = 0;
var freshness = 1;
var shoeGroup;
var tutorialsOn = true;
var shoe_color1 = 0xffffff;
var shoe_color2 = 0xffffff;
var shoe_color3 = 0xffffff;
var followersReq2 = 700;
var followersReq3 = 1300;
var followersReq4 = 2000;

var store2locked = true;
var store3locked = true;
var store4locked = true;

var won = false;
var posted = false;
var polished = false;

var cpmWalk = 0;
var cpmSocial = 0;
var shoeValue = 100;
//var shoe_color4 = 0xf7941d;

var menu = function(game){
    var pauseText;
};

menu.prototype = {

    create: function(){

        game.add.image(0, 0, 'title');

		/*game.add.button(game.world.centerX-128,game.world.centerY,'button',function(button, pointer, isOver){
			if(isOver)
				game.state.start('StudioSelect');
		},null,2,1,0);*/

        // draws spacebar text
        pauseText = game.add.text(game.width/2, game.height*(9/10),
            "PRESS SPACEBAR TO START", {font: "36px Schoolbell", fill: "#FFFFFF"});
        pauseText.anchor.setTo(0.5, 0.5);
       // pauseText.alpha = 0.9;

        // handle spacebar input
    	game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR).onDown.add(
            function(){
                game.input.keyboard.onDownCallback = null;
                game.state.start('StudioSelect');
            }, this);

		this.game.input.keyboard.addKeyCapture(Phaser.Keyboard.BACKSPACE);
	},

    update: function(){
        if (pauseText.alpha > 0.9){
            game.add.tween(pauseText).to({alpha: 0.1}, 1500, Phaser.Easing.Linear.None, true);
        } else if (pauseText.alpha < 0.2){
            game.add.tween(pauseText).to({alpha: 1}, 1500, Phaser.Easing.Linear.None, true);
        }
    }
}
