var polish = function(game){
//All Polish Specific Vars go here
//example
//this.text = 'hello';
    var key_a;
    var key_s;
    var key_d;
    var key_f;
    var key_j;
    var key_k;
    var key_l;
    var key_semi;
    
    var a_button;
    var s_button;
    var d_button;
    var f_button;
    var j_button;
    var k_button;
    var l_button;
    var semi_button;

    var timer;
    var score;

    var background;
    var multiplier;
    
    var emitter;
    var particle_color;
};

polish.prototype = {

	create: function(){
		this.initializeDifficulty();


		//allow physics in this.game
	    this.game.physics.startSystem(Phaser.Physics.ARCADE);

	    //set background
	    background = this.game.add.tileSprite(0, 0, this.game.width, this.game.height,'bkg');
        columns = this.game.add.image(0, 0,'columns');
        columns.alpha = .3;

	    stage = this.game.add.tileSprite(0, 6.5*background.height/8, background.width, background.height,'stage');

	    //score


        this.createShoe();
        this.initializeKeys();
        this.initializeButtons();
        
        emitter = game.add.emitter(0, 0, 200);
        emitter.minParticleScale = emitter.maxParticleScale = 0.075;
        emitter.makeParticles('button_particle');
        particle_color = 0xffffff;


	    falling_buttons = this.game.add.group();
	    if (tutorialsOn) { var tutorial = new Pause('tutorial', {}); }
	    this.releaseButtons();
	},
    
    initializeDifficulty: function(){
        timer = 0;
		score = 0;
        multiplier = 7 - studio_level;
        speed = 50 + 18*studio_level;
        switch(studio_level){
                case 1:
                    num_falling_buttons = 1;
                    break;
                case 2:
                    num_falling_buttons = 2;
                    break;
                case 3:
                    num_falling_buttons = 2;
                    break;
                case 4:
                    num_falling_buttons = 3;
                    break;
        }
        spawning_rate = 1500 - 200*studio_level;
        //console.log(freshness);
        score = freshness*100;
    },

    initializeButtons: function(){
        var scale_buttons = .3;
	    var button_height = background.height - 100;

	    a_button = this.game.add.sprite(20, button_height, 'a_button');
	    a_button.scale.set(scale_buttons);
        a_button.anchor.setTo(0.5, 0.5);
        a_button.x += a_button.width/2;
        a_button.y += a_button.height/2;

	    s_button = this.game.add.sprite(20+background.width/8, button_height, 's_button');
	    s_button.scale.set(scale_buttons);
        s_button.anchor.setTo(0.5, 0.5);
        s_button.x += s_button.width/2;
        s_button.y += s_button.height/2;

	    d_button = this.game.add.sprite(20+ 2*background.width/8, button_height, 'd_button');
	    d_button.scale.set(scale_buttons);
        d_button.anchor.setTo(0.5, 0.5);
        d_button.x += d_button.width/2;
        d_button.y += d_button.height/2;

	    f_button = this.game.add.sprite(20+ 3*background.width/8,button_height, 'f_button');
	    f_button.scale.set(scale_buttons);
        f_button.anchor.setTo(0.5, 0.5);
        f_button.x += f_button.width/2;
        f_button.y += f_button.height/2;

	    j_button = this.game.add.sprite(20+ 4*background.width/8, button_height, 'j_button');
	    j_button.scale.set(scale_buttons);
        j_button.anchor.setTo(0.5, 0.5);
        j_button.x += j_button.width/2;
        j_button.y += j_button.height/2;

	    k_button = this.game.add.sprite(20+ 5*background.width/8, button_height, 'k_button');
	    k_button.scale.set(scale_buttons);
        k_button.anchor.setTo(0.5, 0.5);
        k_button.x += k_button.width/2;
        k_button.y += k_button.height/2;

	    l_button = this.game.add.sprite(20+ 6*background.width/8, button_height, 'l_button');
	    l_button.scale.set(scale_buttons);
        l_button.anchor.setTo(0.5, 0.5);
        l_button.x += l_button.width/2;
        l_button.y += l_button.height/2;

	    semi_button = this.game.add.sprite(20+ 7*background.width/8, button_height, 'semi_button');
	    semi_button.scale.set(scale_buttons);
        semi_button.anchor.setTo(0.5, 0.5);
        semi_button.x += semi_button.width/2;
        semi_button.y += semi_button.height/2;
    },
    
    initializeKeys: function(){
        key_a = this.game.input.keyboard.addKey(Phaser.Keyboard.A);
	    key_a.onDown.add(this.collision, this);
        key_a.onUp.add(this.resize,this);

	    key_s = this.game.input.keyboard.addKey(Phaser.Keyboard.S);
	    key_s.onDown.add(this.collision, this);
        key_s.onUp.add(this.resize,this);


	    key_d = this.game.input.keyboard.addKey(Phaser.Keyboard.D);
	    key_d.onDown.add(this.collision, this);
        key_d.onUp.add(this.resize,this);

        key_f = this.game.input.keyboard.addKey(Phaser.Keyboard.F);
	    key_f.onDown.add(this.collision, this);
        key_f.onUp.add(this.resize,this);
	    
        key_j = this.game.input.keyboard.addKey(Phaser.Keyboard.J);
	    key_j.onDown.add(this.collision, this);
        key_j.onUp.add(this.resize,this);
	    
        key_k = this.game.input.keyboard.addKey(Phaser.Keyboard.K);
	    key_k.onDown.add(this.collision, this);
        key_k.onUp.add(this.resize,this);
	    
        key_l = this.game.input.keyboard.addKey(Phaser.Keyboard.L);
	    key_l.onDown.add(this.collision, this);
        key_l.onUp.add(this.resize,this);
	    
        key_semi = this.game.input.keyboard.addKey(Phaser.Keyboard.COLON);
	    key_semi.onDown.add(this.collision, this);
        key_semi.onUp.add(this.resize,this);
    },

    createShoe: function(){
        var shoe_part1;
        var shoe_part2;
        var shoe_part3;
        switch(studio_level){
                case 1:

                    shoe_part1 = game.add.sprite(background.width/2-50, background.height/2-50, 'shoe1_part1');
                    shoe_part1.anchor.set(0.5);
                    shoe_part2 = game.add.sprite(shoe_part1.x - 182, shoe_part1.y - 40, 'shoe1_part2');
                    shoe_part3 = game.add.sprite(shoe_part1.x - 220, shoe_part1.y + 30, 'shoe1_part3');

                    break;

                case 2:

                    shoe_part1 = game.add.sprite(background.width/2-50, background.height/2-50, 'shoe2_part1');
                    shoe_part1.anchor.set(0.5);
                    shoe_part2 = game.add.sprite(shoe_part1.x - 224, shoe_part1.y - 145, 'shoe2_part2');
                    shoe_part3 = game.add.sprite(shoe_part1.x - 233, shoe_part1.y + 45, 'shoe2_part3');

                    break;
                case 3:

                    shoe_part1 = game.add.sprite(background.width/2-50, background.height/2-50, 'shoe3_part1');
                    shoe_part1.anchor.set(0.5);
                    shoe_part2 = game.add.sprite(shoe_part1.x - 178, shoe_part1.y - 65, 'shoe3_part2');
                    shoe_part3 = game.add.sprite(shoe_part1.x - 224, shoe_part1.y + 40, 'shoe3_part3');

                    break;
                case 4:
                    shoe_part1 = game.add.sprite(background.width/2-50, background.height/2-50, 'shoe4_part1');
                    shoe_part1.anchor.set(0.5);
                    shoe_part2 = game.add.sprite(shoe_part1.x - 120, shoe_part1.y - 160, 'shoe4_part2');
                    shoe_part3 = game.add.sprite(shoe_part1.x - 210, shoe_part1.y - 105, 'shoe4_part3');

                    break;
        }
            shoe_part1.tint = shoe_color1;
            shoe_part2.tint = shoe_color2;
            shoe_part3.tint = shoe_color3;

            shoeGroup = game.add.group();
            shoeGroup.add(shoe_part1);
            shoeGroup.add(shoe_part2);
            shoeGroup.add(shoe_part3);

        t1 = game.add.tween(shoeGroup.scale);
        t1.to( {x: 1.2, y: 1.2}, 1000, Phaser.Easing.Back.InOut, true, 0, false).yoyo(true);
		
		this.uiBar1 = new uiBar("polish");
		this.uiBar1.initialize();
    },

    
    resize: function(key){
        if(key == key_a)
                a_button.scale.set(.3);
        if(key == key_s)
                s_button.scale.set(.3);
        if(key == key_d)
                d_button.scale.set(.3);
        if(key == key_f)
                f_button.scale.set(.3);
        if(key == key_j)
                j_button.scale.set(.3);
        if(key == key_k)
                k_button.scale.set(.3);
        if(key == key_l)
                l_button.scale.set(.3);
        if(key == key_semi)
                semi_button.scale.set(.3);
            
    },
    
    collision: function(key){
        var button_height = background.height - 100;
        var incr1, incr2, color_num;
        var curr_button;        

        if(key == key_a){
            incr1 = 0;
            incr2 = 1;
            a_button.scale.set(.35);
            curr_button = a_button;
            color_num = 1;

        }
        if(key == key_s){
            incr1 = 1;
            incr2 = 2;
            s_button.scale.set(.35);
            curr_button = s_button;
            color_num = 2;

        }
        if(key == key_d){
            incr1 = 2;
            incr2 = 3;
            d_button.scale.set(.35);
            curr_button = d_button;
            color_num = 3;
        }
        if(key == key_f){
            incr1 = 3;
            incr2 = 4;
            f_button.scale.set(.35);
            curr_button = f_button;
            color_num = 4;
        }
        if(key == key_j){
            incr1 = 4;
            incr2 = 5;
            j_button.scale.set(.35);
            curr_button = j_button;
            color_num = 4;
        }
        if(key == key_k){
            incr1 = 5;
            incr2 = 6;
            k_button.scale.set(.35);
            curr_button = k_button;
            color_num = 3;
        }
        if(key == key_l){
            incr1 = 6;
            incr2 = 7;
            l_button.scale.set(.35);
            curr_button = l_button;
            color_num = 2;
        }
        if(key == key_semi){
            incr1 = 7;
            incr2 = 8;
            semi_button.scale.set(.35);
            curr_button = semi_button;
            color_num = 1;        
        }
        
        emitter.x = curr_button.x ;
        emitter.y = curr_button.y ;
        
        falling_buttons.forEach(function(button){
	     if(button.body.y > button_height -40
	          && button.body.y < button_height +40
	        && button.body.x >= 20+incr1*background.width/8
	        && button.body.x < 20+incr2*background.width/8){
	            falling_buttons.remove(button,true,false);
	            score += 7-studio_level;
             
             
                switch(color_num){
                        case 1:
                            particle_color = 0x7f30c8;
                        break;
                        case 2:
                            particle_color = 0x3d6a28;
                        break;
                        case 3:
                             particle_color = 0x1bc1d9;
                        break;
                        case 4:
                             particle_color = 0xff3074;
                        break;             
                }
              emitter.forEach(function(particle) {
                  particle.tint = particle_color;
                });
                emitter.explode(1000, 7);

	        }
	    },this);
        

        
    },

	releaseButtons: function(){
	    //select which button to drop
	    var random = (Math.ceil((Math.random()*8)));

        //number of buttons falling at one time
	    var num_buttons = (Math.ceil((Math.random()*num_falling_buttons)));
	   for(i = 0; i < num_buttons; i++){
	        this.makeButtons(random);
	        new_random = (Math.ceil((Math.random()*8)));
	        while( new_random == random){
	               new_random = (Math.ceil((Math.random()*8)));
	        }
	        random = new_random;
	    }

	    //delay between new buttons spawning
	    timer = this.game.time.now + 1500;
	},

	makeButtons: function(random){
	    switch(random){
	            case 1:
	            button = falling_buttons.create(20, 0, 'polish_purple');
	            break;
	            case 2:
	            button = falling_buttons.create(20+background.width/8, 0, 'polish_green');
	            break;
	            case 3:
	            button = falling_buttons.create(20+2*background.width/8, 0, 'polish_blue');
	            break;
	            case 4:
	            button = falling_buttons.create(20+3*background.width/8, 0, 'polish_red');
	            break;
	            case 5:
	            button = falling_buttons.create(20+4*background.width/8, 0, 'polish_red');
	            break;
	            case 6:
	            button = falling_buttons.create(20+5*background.width/8, 0, 'polish_blue');
	            break;
	            case 7:
	            button = falling_buttons.create(20+6*background.width/8, 0, 'polish_green');
	            break;
	            case 8:
	            button = falling_buttons.create(20+7*background.width/8, 0, 'polish_purple');
	            break;
	       }
	    button.scale.set(.3);
	    button.enableBody = true;
	    this.game.physics.arcade.enable(button);
	    button.body.collideWorldBounds = true;

	    //change speed of buttons falling
	    button.body.velocity.y = speed;
	},

	update: function() {
	    if(this.game.time.now >timer){
	        this.releaseButtons();
	    }

	    falling_buttons.forEach(function(button){
	        if(button.body.velocity.y == 0){
	            button.kill();
	        }
	    },this);

	    if(score >= 150){
            console.log(score);
	        this.winGame();
	    }
	    else{
	    }
		freshness = score/100;
		this.uiBar1.update();
	},

	winGame: function(){
	    falling_buttons.destroy();
        if (score >= 150){
            score = 150;
        }
        freshness = score/100;
        polished = true;
        var scores = [];
        scores[0] = followers;
        scores[1] = cpmSocial;
        scores[2] = cpmWalk;
        scores[3] = shoeValue;
        scores[4] = freshness;
		var pause = new Pause('score', scores);
	}


}
