var hub = function(game){
	var bed;
	var border;
	var carpet;
	var desk;
	var full_table;
	var wall;

	var overDesk;
	var overBed ;
	var overTable;

	var bigBed;
	var bigDesk;
	var bigTable;

	var imageName;
	var imageText;

    var padlock;

    this.alerted = true;
};

hub.prototype = {

	create: function(){
		// initializing variables
		overDesk = false;
		overBed = false;
		overTable = false;
		imageName = "";

		// creating tilesprites for repeating wall, carpet, and border images
		wall = this.game.add.tileSprite(0, 0, this.game.width, this.game.height, 'wall');
		carpet = this.game.add.tileSprite(0, this.game.height*(.45), this.game.width, this.game.height, 'carpet');
    	border = this.game.add.tileSprite(0, this.game.height*(.45), this.game.width, 10, 'border');

    	// creating images and scaling bed, desk and table
		bed = this.game.add.image(this.game.width*(0.63), this.game.height*(0.26), 'bed');
		desk = this.game.add.image(this.game.width*(0.16), this.game.height*(0.22), 'desk');
		table = this.game.add.image(this.game.width*(0.35), this.game.width*(0.55), 'full_table');

		bed.scale.setTo(0.6);
		desk.scale.setTo(0.5);
		table.scale.setTo(0.5);

        bigBed = this.game.add.image(bed.x + bed.width/2, bed.y + bed.height/2, 'bed');
        bigBed.scale.setTo(0.65);
        bigBed.anchor.setTo(0.5, 0.5);

        bigDesk = this.game.add.image(desk.x + desk.width/2, desk.y + desk.height/2, 'desk');
        bigDesk.scale.setTo(0.55);
        bigDesk.anchor.setTo(0.5, 0.5);

        bigTable = this.game.add.image(table.x + table.width/2, table.y + table.height/2, 'full_table');
        bigTable.scale.setTo(0.55);
        bigTable.anchor.setTo(0.5, 0.5);

        bigBed.alpha = 0;
        bigDesk.alpha = 0;
        bigTable.alpha = 0;

		// enabling clicking on images
		bed.inputEnabled = true;
		desk.inputEnabled = true;
		table.inputEnabled = true;

		// uses hand cursor when over these images
		bed.input.useHandCursor = true;
		desk.input.useHandCursor = true;
		table.input.useHandCursor = true;

		// setting callback for clicking on sprites
		bed.events.onInputDown.add(this.onDown);
		desk.events.onInputDown.add(this.onDown);
		table.events.onInputDown.add(this.onDown);

		// draws texts and expands image when mouse is hovering
		bed.events.onInputOver.add(this.hovering);
		bed.events.onInputOut.add(this.notHovering);
		desk.events.onInputOver.add(this.hovering);
		desk.events.onInputOut.add(this.notHovering);
		table.events.onInputOver.add(this.hovering);
		table.events.onInputOut.add(this.notHovering);

		this.uiBar1 = new uiBar("hub");
		this.uiBar1.initialize();
		
        padlock = this.game.add.image(0, 0, 'padlock');
        padlock.scale.setTo(0.2);
        padlock.x = bed.x + bed.width/2 - padlock.width/2;
        padlock.y = bed.y + bed.height/2 - padlock.height/2;
        padlock.alpha = 0;

        checkmark2 = this.game.add.image(0, 0, 'checkmark');
        checkmark2.scale.setTo(0.4);
        checkmark2.x = desk.x + desk.width/2 - checkmark2.width/2;
        checkmark2.y = desk.y + desk.height*(1/3) - checkmark2.height/2;
        checkmark2.alpha = 0;

        if (!posted){
            padlock.alpha = 1;
            bed.tint = "0x525252";
        } else {
            checkmark2.alpha = 1;
            desk.tint = "0x24cc2a";
        }

        checkmark = this.game.add.image(0, 0, 'checkmark');
        checkmark.scale.setTo(0.4);
        checkmark.x = table.x + table.width/2 - checkmark.width/2;
        checkmark.y = table.y + table.height*(1/3) - checkmark.height/2;
        checkmark.alpha = 0;

        if (polished){
            checkmark.alpha = 1;
            table.tint = "0x24cc2a";
        }

		imageText = this.game.add.text(0, 0,
			imageName, {font: "50px Schoolbell", fill: "#FFFFFF", align: "center"});
		imageText.stroke = 'rgba(0, 0, 0, 1)';
		imageText.strokeThickness = 8;

		if (tutorialsOn) {
            var scores = [];
            scores[0] = posted;
            scores[1] = polished;
            console.log(scores[0]);
            var tutorial = new Pause('tutorial', scores);
        }
	},

	update: function(){
		this.uiBar1.update(posted);
		if (overBed){
			this.keepWordsOverCursor(bed);
		} else if (overDesk){
			this.keepWordsOverCursor(desk);
		} else if (overTable){
			this.keepWordsOverCursor(table);
		}
	},

	notHovering: function(image){
		imageText.setText("");
		switch (image.key){
			case 'bed':
				overBed = false;
				bigBed.alpha = 0;
                break;
			case 'desk':
				overDesk = false;
				bigDesk.alpha = 0;
                break;
			case 'full_table':
				overTable = false;
				bigTable.alpha = 0;
                table.alpha = 1;
				break;
		}
	},

	hovering: function(image){
		switch (image.key){
			case 'bed':
                overBed = true;
                if (this.posted){
                    imageName = "Start the next day!";
                    bigBed.alpha = 1;
                } else {
                    imageName = "You must post about \n your shoes first.";
                }
				break;
			case 'desk':
                if (this.posted){
                    imageName = "You already posted!";
                } else {
                    imageName = "Post your shoes online!";
                    bigDesk.alpha = 1;
                }

				overDesk = true;
				break;
			case 'full_table':
				imageName = "Clean your shoes!";
                if (!polished){
                    bigTable.alpha = 1;
                    table.alpha = 0;
                }
				overTable = true;
				break;
		}
		imageText.setText(imageName);
	},

	keepWordsOverCursor: function(image){
		imageText.x = this.game.input.mousePointer.x - imageText.width/2;
		imageText.y = this.game.input.mousePointer.y;
		imageText.anchor.setTo(0, 1);
	},

    showAlert: function(){
        if (this.alerted != true){
            var alertWindow = game.add.image(0, 0, 'alert');
            var alertText = game.add.text(game.width/2, game.height*(2/10) + 15, "You need to post your shoes online before going to bed.\n", {font: "30px Schoolbell", fill: "#FFFFFF"});
            alertText.anchor.setTo(0.5, 0.5);
            var timer = game.time.create();
            this.alerted = true;
            timer.add(3000, function(){
                this.alerted = false;
                alertWindow.destroy();
                alertText.destroy();
            }, this);
            timer.start();
        }
    },

	onDown: function(sprite){
		switch (sprite.key){
			case 'bed':
                if (this.posted){
                    // increment total followers
                    followers += cpmSocial + cpmWalk + Math.ceil(shoeValue * freshness);

                    var scores = [];
                    scores[0] = followers;
                    scores[1] = cpmSocial;
                    scores[2] = cpmWalk;
                    scores[3] = shoeValue;
                    scores[4] = freshness;
                    tutorialsOn = false;
                    var score = new Pause('score', scores);

                    if (studio_level == 4){
                        won = true;
                    }

                    this.game.state.start('StudioSelect');
                } else {
                    hub.prototype.showAlert();
                }
				break;
			case 'desk':
                if (!this.posted){
				    this.game.state.start('SocialMedia');
                }
				break;
			case 'full_table':
                if (!polished){
				    this.game.state.start('Polish');
                }
				break;
		}
	}

}
